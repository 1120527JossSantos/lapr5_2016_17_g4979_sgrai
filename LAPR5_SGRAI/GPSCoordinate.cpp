#include "GPSCoordinate.h"

GPSCoordinate::~GPSCoordinate() { }

float GPSCoordinate::getLatitude() {
	return latitude_;
}

float GPSCoordinate::getLongitude() {
	return longitude_;
}

void GPSCoordinate::setLatitude(const float latitude) {
	latitude_ = latitude;
}

void GPSCoordinate::setLongitude(const float longitude) {
	longitude_ = longitude;
}

void GPSCoordinate::toArray(float* v) {
	v[0] = latitude_;
	v[1] = longitude_;
}