#pragma once

#include "Coordinate.h"

#define LATITUDE 0
#define LONGITUDE 1

class GPSCoordinate : virtual Coordinate {
private:
	// ----- LATITUDE  = 41.1443339 -> 1443339
	// ----- LONGITUDE = -8.5868845 -> 5868845
	float latitude_;  // 41.xxxxxxx ==/== xxxxxxxx 
	float longitude_; // -8.xxxxxxx

public:
	GPSCoordinate(float lat, float lon) :
		latitude_(lat), longitude_(lon) {};
	GPSCoordinate() : latitude_(0), longitude_(0) {};
	~GPSCoordinate();

	float getLatitude();
	float getLongitude();

	void setLatitude(const float latitude);
	void setLongitude(const float longitude);

	virtual void toArray(float* v);
};