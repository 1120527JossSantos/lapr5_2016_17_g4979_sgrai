#include "Importer.h"


// nota std::cout apesar de using namespace std por causa de shenanigans do VS
using namespace std;

// ---------------------------------------------------------------
Importer::Importer() {
	// conexao com db
	this->flagLoadData = false;
	this->flagNodes = false;
	this->flagWays = false;

	this->flagLoaderAPIRest = false;
	this->flagLoaderTextFile = false;
}

Importer::Importer(std::string LOADER_FLAG) {
	this->flagLoadData = false;
	this->flagNodes = false;
	this->flagWays = false;

	if (LOADER_FLAG.compare(__FLAG_LOADER_TEXT_FILE) == 0) {
		this->flagLoaderTextFile = true;
		this->flagLoaderAPIRest = false;
	}
	else if (LOADER_FLAG.compare(__FLAG_LOADER_API_REST) == 0) {
		this->flagLoaderTextFile = false;
		this->flagLoaderAPIRest = true;
	}
	else {
		this->flagLoaderTextFile = false;
		this->flagLoaderAPIRest = false;
	}
}

Importer::~Importer() {

}
// ---------------------------------------------------------------

vector<Node> * Importer::loadData(vector<Node>* node_list)
{
	// nota std::cout apesar de using namespace std por causa de shenanigans do VS
	std::cout << "Importer::loadData()" << endl;
	std::cout << "Loading TextFile MAP Data ..." << endl;
	if (flagLoaderTextFile) {
		std::cout << __XML_FILE << "  MAP Data " << endl;
					
		xml_document<> doc;
		xml_node<> * root_node;

		// Read XML into vector
		ifstream xmlFile(__XML_FILE);
		vector<char> buffer((istreambuf_iterator<char>(xmlFile)), istreambuf_iterator<char>());
		
		
		buffer.push_back('\0');
		// Parse buffer using xml file parsing 
		doc.parse<0>(&buffer[0]);
		// Find root node
		root_node = doc.first_node("osm");
		// Iterate over the Nodes to List Them
		int countNodes = 0;
		for (xml_node<> * sgrai_node = root_node->first_node("node"); sgrai_node; sgrai_node = sgrai_node->next_sibling())
		{


			printf("I have visited node %s with lat %s and long %s. ",
				sgrai_node->first_attribute("id")->value(),
				sgrai_node->first_attribute("lat")->value(),
				sgrai_node->first_attribute("lon")->value());

			std::cout << endl;
			++countNodes;
		}

		// Iterate over the Nodes to List Them
		for (xml_node<> * sgrai_node = root_node->first_node("node"); sgrai_node; sgrai_node = sgrai_node->next_sibling())
		{
			//Node(int id, string Name, int VisitTime,
			//	GPSCoordinate gps)
			//	: id_(id), Name_(Name),
			//	gps_coord_(gps), cart_coord_() {}; // CONST para testes

			// ----------------I idNode
			char * text = sgrai_node->first_attribute("id")->value();
			int idNode = 0;
			std::string textS (text);
			std::string::size_type sz;
			idNode = std::stoi(textS, &sz);
			// ----------------I idNode
			text = sgrai_node->first_attribute("name")->value();
			std::string nameNode(text);
			// ----------------I gpsCoordNode
			text = sgrai_node->first_attribute("lat")->value();
			std::string latS(text);
			float latNode = std::stof(latS);

			text = sgrai_node->first_attribute("lon")->value();
			std::string lonS(text);
			float lonNode = std::stof(lonS);

			GPSCoordinate gpsNode(latNode, lonNode);
			// --------------------------------------------------------------------------------- TODO para o ficheiro que vamos receber
			// --------------------------------------------------------------------------------- TODO para o ficheiro que vamos receber
			// --------------------------------------------------------------------------------- TODO para o ficheiro que vamos receber
			cout << std::setprecision(7) << std::fixed; cout << "Converting node  " << idNode <<
				"  with name  "<< nameNode <<
				"  and GPSLong  " <<gpsNode.getLongitude() <<
				"  and GPSLat  " << gpsNode.getLatitude()<< "  ." << endl;
		}

		std::cout << "Text File MAP Data Loaded.  "<< countNodes <<" Nodes have been loaded.  " << endl << endl;
		flagLoadData = true;
		std::cout << "flagLoadData = true" << endl << endl;
	}
	else if (flagLoaderAPIRest) { 
		// ----------------------------------------------- LOADER API REST

		this->nodeList = new vector<Node>();
		nodeList->clear();

		std::cout << __JSON_FILE_NODES << "  MAP Data "  << endl;
		
		std::string::size_type sz;   // alias of size_t

		Document document;
		ifstream jsonFile(__JSON_FILE_NODES);
		std::string contents((std::istreambuf_iterator<char>(jsonFile)),
			std::istreambuf_iterator<char>());
		document.Parse(contents.c_str());

		assert(document.IsObject());
		assert(document.HasMember("POI"));
		assert(document["POI"].IsArray());
		const Value& poiArr = document["POI"];
		
		for (SizeType i = 0; i < poiArr.Size(); ++i) {
			assert(poiArr[i].IsObject());
			assert(poiArr[i].HasMember("ID"));
			std::cout << "Loading Node " << i << " with ID : " << poiArr[i]["ID"].GetString() << endl;

			//double temp = ::atof(num.c_str());

			
			// TOKENS ISTRINGSTREAM FOR LAT
			istringstream issLAT(poiArr[i]["Latitude"].GetString());
			std::vector<std::string> tokensLAT;
			std::string tokenLAT;
			while (std::getline(issLAT, tokenLAT, '.')) {
				if (!tokenLAT.empty()) {
					tokensLAT.push_back(tokenLAT);
				}
			}
			// TOKENS ISTRINGSTREAM FOR LONG
			istringstream issLONG(poiArr[i]["Longitude"].GetString());
			std::vector<std::string> tokensLONG;
			std::string tokenLONG;
			while (std::getline(issLONG, tokenLONG, '.')) {
				if (!tokenLONG.empty()) {
					tokensLONG.push_back(tokenLONG);
				}
			}


			Node n = Node(std::stoi(poiArr[i]["ID"].GetString(), &sz),
				poiArr[i]["Name"].GetString(),
				std::atof(tokensLAT[1].c_str()),
				std::atof(tokensLONG[1].c_str()));

			n.setCoordinatesCart(CoordinateConversor::toCartesianValues(n.getCoordinatesGPS().getLatitude(),
				n.getCoordinatesGPS().getLongitude()));
			n.setAltitude(std::stoi(poiArr[i]["Altitude"].GetString()));
			n.setDescription(poiArr[i]["Description"].GetString());
			node_list->push_back(n);

			
			
			
			std::cout << "Loaded  Node   with ID   " << n.getId() << "  Named  " << n.getName() << "  Lat  " <<
				 n.getCoordinatesGPS().getLatitude()
				<< "  Lon  " << n.getCoordinatesGPS().getLongitude() << "  ." << endl;
			std::cout << "Cartesian    X  " << n.getCoordinatesCart().getX()
				<< "  Y  " << n.getCoordinatesCart().getY() << "  Z  " << n.getAltitude()
				<< "  ." << endl;
		}
		

		flagLoadData = true;
		// ----------------------------------------------- LOADER API REST
	}

	return nodeList; // TODO : conexao com db
}

bool Importer::loadWays(vector<Way> * way_list)
{
	cout << "Importer::Ways()" << endl;
	if (flagLoadData) {
		cout << "flagLoadData == true"<<endl;

			std::cout << "Loading TextFile MAP Data ..." << endl;
			// ----------------------------------------------- LOADER API REST

			this->wayList = new vector<Way>();
			wayList->clear();

			std::cout << __JSON_FILE_NODES << "  MAP Data " << endl;

			std::string::size_type sz;   // alias of size_t

			Document document;
			ifstream jsonFile(__JSON_FILE_NODES);
			std::string contents((std::istreambuf_iterator<char>(jsonFile)),
				std::istreambuf_iterator<char>());
			document.Parse(contents.c_str());

			assert(document.IsObject());
			assert(document.HasMember("Connection"));
			assert(document["Connection"].IsArray());
			const Value& wayArr = document["Connection"];

			for (SizeType i = 0; i < wayArr.Size(); ++i) {
				assert(wayArr[i].IsObject());
				assert(wayArr[i].HasMember("POI1ID"));
				assert(wayArr[i].HasMember("POI2ID"));
				assert(wayArr[i].HasMember("Distance"));
				std::cout << "Loading Way " << i << " with POI1ID : " << wayArr[i]["POI1ID"].GetString()
					<< "  and POI2ID : "<< wayArr[i]["POI2ID"].GetString()
					<< "  Distanced  " << wayArr[i]["Distance"].GetString() << " meters ." << endl;

				//double temp = ::atof(num.c_str());

				
				Way w =  Way(std::atoi(wayArr[i]["POI1ID"].GetString()),
					std::atoi(wayArr[i]["POI2ID"].GetString()),
					std::atoi(wayArr[i]["Distance"].GetString()));
				
				

				way_list->push_back(w);



				std::cout << "Loaded  Way "<< i <<" with POI1ID   " << w.getPOI1ID() << "  and POI2ID   " << w.getPOI2ID() <<
					"  Distanced  " << w.getDistance() << endl;
			}


			flagLoadData = true;
		
		}

	return way_list;
}

bool Importer::loadVisits(vector<Visit>* visit_list)
{
	cout << "Importer::Visits()" << endl;
	if (flagLoadData) {
		cout << "flagLoadData == true" << endl;

		std::cout << "Loading TextFile MAP Data ..." << endl;
		// ----------------------------------------------- LOADER API REST

		this->visitList = new vector<Visit>();
		visitList->clear();

		std::cout << __JSON_FILE_NODE_VISITS << "  MAP Data " << endl;

		std::string::size_type sz;   // alias of size_t

		Document document;
		ifstream jsonFile(__JSON_FILE_NODE_VISITS);
		std::string contents((std::istreambuf_iterator<char>(jsonFile)),
			std::istreambuf_iterator<char>());
		document.Parse(contents.c_str());

		assert(document.IsObject());
		assert(document.HasMember("POI"));
		assert(document["POI"].IsArray());
		const Value& wayArr = document["POI"];

		std::cout << "Loading Current User Visit Route ... " << std::endl;

		for (SizeType i = 0; i < wayArr.Size(); ++i) {
			assert(wayArr[i].IsObject());
			assert(wayArr[i].HasMember("ID1"));
			assert(wayArr[i].HasMember("ID2"));

			std::cout << "Loading Visit trench that starts at POI " << wayArr[i]["ID1"].GetString() << " and goes to POI "
				<< wayArr[i]["ID2"].GetString() << " by " << wayArr[i]["Type"].GetString() << " to the next one. " << std::endl;


			//double temp = ::atof(num.c_str());


		/*	Way w = Way(std::atoi(wayArr[i]["POI1ID"].GetString()),
				std::atoi(wayArr[i]["POI2ID"].GetString()),
				std::atoi(wayArr[i]["Distance"].GetString()));*/

			bool flagFoot = false;
			if (string("Foot").compare(wayArr[i]["Type"].GetString()) == 0) flagFoot = true;
			
			std::cout << " flagFoot  " << flagFoot << endl;

			Visit v =  Visit(std::atoi(wayArr[i]["ID1"].GetString()),
				std::atoi(wayArr[i]["ID2"].GetString()), flagFoot);

			visit_list->push_back(v);
			
			
		}


		flagLoadData = true;

	}


	return false;
}





std::vector<Node> Importer::getNodes()
{
	return std::vector<Node>();
}

std::vector<Way> Importer::getWays()
{
	return std::vector<Way>();
}

std::string Importer::generateWaysDescription(std::vector<Visit>* visit_list)
{
	std::string description = string("  Current user visit: ");
	stringstream ss;
	ss << description;

	for (std::vector<Visit>::iterator it = visit_list->begin(); it != visit_list->end(); ++it) {
		// TODO
	}

	return std::string();
}
