#pragma once
#include "CartCoordinate.h"
#include "GPSCoordinate.h"

#define LAT_MIN 141385
#define LON_MIN 583595
#define MULTIPLIER 0.017

class CoordinateConversor {
public:
	static GPSCoordinate toGPS(CartCoordinate* cart);
	static CartCoordinate toCartesian(GPSCoordinate* gps);
	static CartCoordinate toCartesianValues(float lat, float lon);
};

