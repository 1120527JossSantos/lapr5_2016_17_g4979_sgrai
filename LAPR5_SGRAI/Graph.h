#pragma once

#include <math.h>
#include <glut.h>
#include <string>
#include <iostream>
#include <sys/stat.h>
#include <sys/types.h>

#include "mathlib.h"
#include "studio.h"
#include "mdlviewer.h"
#include "TextureLoader.h"
#include "Model_3DS.h"
#include "CartCoordinate.h"

#pragma comment( lib, "glaux.lib" )

#define MAX_NODES 80
#define MAX_WAYS 80

#define K_NODE 2.1
#define K_CONNECTION 1.25

#define MODEL_ROTATION 4
#define MODEL_SIZE 0.004

#define NUM_MODELS 2

#define M_PI 3.14159265
#define radians(x) M_PI * x / 180
#define degrees(x) x * 180 / M_PI

enum ModelType { HOMER_MODEL, CAR_MODEL };
enum ObjectType { NULL_OBJECT, NODE, WAY };
enum CameraType { FIRST_PERSON, THIRD_PERSON };

typedef struct {
	int id;
	CartCoordinate* coords;
	float radius;
	float s;
	std::string name;
	std::string description;
	glTexture textureWall;
	Model_3DS poi_model;
} No;

typedef struct {
	No *ni, *nf;
	std::string name;
	float weight;
	float width;
	float length;
	float projection;
	float orientation;
	float steepness;
	glTexture textureGround;
	glTexture textureWall;
	glTexture textureSideWalk;
	bool belongsVisit;
	bool isFoot;
} Arco;

typedef struct {
	GLfloat fov;
	GLfloat player_dist;
	GLfloat view_dist;
	GLdouble dir_lat;
	GLdouble dir_long;
	CameraType type;
} Camera;

typedef struct {
	ObjectType on_obj;
	int on_obj_id;
	ModelType current_model;
	bool travelling;
	int travelling_from;
} PlayerState;

typedef struct {
	CartCoordinate coords;
	float orientation;
	float velocity;
	PlayerState state;
} Player;

class Graph {
private:
	
	Camera camera;
	No nodes_[MAX_NODES];
	Arco ways_[MAX_WAYS];
	int numNodes_ = 0, numWays_ = 0;
	bool showTooltip;
	std::string visit_description;

	int modelExists(const char* path);
	void renderNodeConnection(No* node, float orientation, float width, float length);
	GLfloat normalize(GLfloat v[]);
public:
	Player player;

	Graph();
	~Graph();

	void addNode(int id, std::string name,std::string description, float x, float y, float z, float w1, glTexture textureWall);
	void addWay(int id1, int id2, std::string name, float weight, float width, glTexture texture,
		glTexture textureWall, glTexture sidewalk, bool belongsVisit, bool isFoot);

	float getDistance(float* from, float* to);
	No* getNode(int id);
	Arco* getWay(int id);
	Player* getPlayer();
	Camera* getCamera();

	void updateCamera();
	void updatePerspective();

	void changeModel();
	void changeCamera();
	void setCameraType(CameraType type);
	void incOrientation(float inc);

	void listNodes(){
		for (int i = 0; i < numNodes_; i++) {
			std::cout << "Node with ID " << nodes_[i].id;
		}
	}

	void renderGround(float* color, bool centerPlayer = true);
	void renderPlayer(StudioModel* model, bool minimap = false);
	void renderNodes();
	void renderWays();
	void renderSkyBox(glTexture* texture);

	void setShowTooltip(bool value) { this->showTooltip = value; };
	bool getShowTooltip() { return this->showTooltip; };
	void setVisitDescription(std::string description) { this->visit_description = description; };
	std::string getVisitDescription() { return this->visit_description; };

	
};