#pragma once

class Coordinate {
public:
	Coordinate() {}
	~Coordinate() {}

	virtual void toArray(float* v) = 0;
};