#include "Node.h"

// CONST ------------------------------------------------------------
// DONE IN "Node.h"

// GET ------------------------------------------------------------

Node::Node(int id, string Name, float lat, float lon)
{
	this->id_ = id;
	this->Name_ = Name;
	this->gps_coord_ = GPSCoordinate(lat, lon);
	
}

int Node::getId() {
	return id_;
}

string Node::getName() {
	return this->Name_;
}

int Node::getVisitTime() {
	return this->VisitTime_;
}

CartCoordinate Node::getCoordinatesCart() {
	return cart_coord_;
}

GPSCoordinate Node::getCoordinatesGPS() {
	return gps_coord_;
}

// SET ------------------------------------------------------------

void Node::setId(const int id) {
	id_ = id;
}

void Node::setCoordinatesCart(const CartCoordinate cart) {
	cart_coord_ = cart;
}

void Node::setCoordinatesCart(float x, float y, float z)
{
	this->cart_coord_ = CartCoordinate(x, y, z);
}

void Node::setCoordinatesGPS(const GPSCoordinate gps) {
	gps_coord_ = gps;
}