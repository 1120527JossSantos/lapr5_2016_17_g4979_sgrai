#pragma once
#include "CartCoordinate.h"
#include "GPSCoordinate.h"
#include <string>

using namespace std;

class Node {
private:
	int id_;
	string Name_;
	string Description_;
	int VisitTime_; // minutes
	string Restrictions_; // descricao das restricoes -> TODO atraves REST API
	CartCoordinate cart_coord_;
	GPSCoordinate gps_coord_;
	int altitude_;
public:
	// CONST ---------------------------
	Node(int id, CartCoordinate coords, GPSCoordinate gps) : id_(id), cart_coord_(coords), gps_coord_(gps) {}
	Node(int id, string Name, int VisitTime,
		CartCoordinate coords, GPSCoordinate gps,
		string Restrictions)
		: id_(id), Name_(Name), VisitTime_(VisitTime),
		cart_coord_(coords), gps_coord_(gps),
		Restrictions_(Restrictions) {}; // MAIN CONST

	Node(int id, string Name,
		GPSCoordinate gps)
		: id_(id), Name_(Name), 
		gps_coord_(gps), cart_coord_() {}; // CONST para testes
	Node(int  id, string Name, float lat, float lon);
	~Node() {};

	// GET   ---------------------------
	int getId();
	CartCoordinate getCoordinatesCart();
	GPSCoordinate getCoordinatesGPS();
	int getAltitude() { return this->altitude_; };
	string getName();
	int getVisitTime();
	string getDescription() { return this->Description_; };

	// SET   ---------------------------
	// todo return BOOLS from SET to validate
	void setId(const int id);
	void setCoordinatesCart(const CartCoordinate cart);
	void setCoordinatesCart(float x, float y, float z);
	void setCoordinatesGPS(const GPSCoordinate gps);
	void setName(const string name);
	void setAltitude(int altitude) { this->altitude_ = altitude; };
	void setDescription(string description) { this->Description_ = description; };

	// VALID --------------------------- TODO
};