#include "CoordinateConversor.h"

GPSCoordinate CoordinateConversor::toGPS(CartCoordinate* cart) {
	float lon = LON_MIN + (cart->getX() / MULTIPLIER);
	float lat = LAT_MIN + (cart->getY() / MULTIPLIER);
	return GPSCoordinate(lat, lon);
}

CartCoordinate CoordinateConversor::toCartesianValues(float lat, float lon) {
	float y = (lat - LAT_MIN) * MULTIPLIER;
	float x = -(lon - LON_MIN) * MULTIPLIER;
	return CartCoordinate(x, y, 0.0);
}

CartCoordinate CoordinateConversor::toCartesian(GPSCoordinate* gps) {
	float x = -(gps->getLongitude() - LON_MIN) * MULTIPLIER;
	float y = (gps->getLatitude() - LAT_MIN) * MULTIPLIER;
	return CartCoordinate(x, y, 0.0);
}