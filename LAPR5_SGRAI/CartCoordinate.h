#pragma once

#include "Coordinate.h"

#define X 0
#define Y 1
#define Z 2

class CartCoordinate : virtual Coordinate {
private:
	float x_, y_, z_;
public:
	CartCoordinate(const float x, const float y, const float z) :
		x_(x), y_(y), z_(z) {};
	CartCoordinate();
	~CartCoordinate() {};

	float getX();
	float getY();
	float getZ();

	void setX(const float x);
	void setY(const float y);
	void setZ(const float z);

	virtual void toArray(float* v);
};