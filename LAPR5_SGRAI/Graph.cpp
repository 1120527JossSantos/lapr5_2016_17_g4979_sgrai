#include "Graph.h"

Graph::Graph() {
	player.orientation = 0.0f;
	player.velocity = 0.02f;
	player.state.current_model = HOMER_MODEL;
	player.state.on_obj = NULL_OBJECT;
	player.state.on_obj_id = -1;
	player.state.travelling = true;
	player.velocity = 0.02f;
	player.state.current_model = HOMER_MODEL;

	camera.fov = 60.0f;
	camera.player_dist = 1.0f;
	camera.view_dist = 50.0f;
	camera.dir_lat = 0.0f;
	camera.dir_long = 0.0f;
	setCameraType(THIRD_PERSON);
	showTooltip = false;
}

Graph::~Graph() {}

GLfloat Graph::normalize(GLfloat v[]) {
	int	i;
	GLdouble length;

	if (fabs(v[1] - 0.000215956) < 0.0001)
		i = 1;

	length = 0;
	for (i = 0; i< 3; i++)
		length += v[i] * v[i];
	length = sqrt(length);
	if (length == 0)
		return 0;

	for (i = 0; i< 3; i++)
		v[i] /= length;

	return degrees( atan2(v[1], v[0]) );
}

int Graph::modelExists(const char* path) {
	struct stat info;
	stat(path, &info);
	if (info.st_size)
		return 1;
	return 0;
}

void Graph::addNode(int id, std::string name, std::string description, float x, float y, float z, float w1, glTexture textureWall) {
	std::cout << "Graph::addNode() "<< id << "with X " << x << " and alt " << z <<std::endl;
	if (numNodes_ == MAX_NODES)	return;
	const int buf_size = 1024;
	char buf[buf_size];
	strcpy_s(buf, buf_size, "./Models/poi/");
	strcat_s(buf, buf_size, std::to_string(id).c_str());
	strcat_s(buf, buf_size, "/Model.3ds\0");
	No* node = &nodes_[numNodes_];
	node->id = id;
	node->coords = new CartCoordinate(x, y, z + 0.1f);
	node->radius = K_NODE * w1 / 2.0f;
	node->s = node->radius * K_CONNECTION;
	node->name = (sizeof name == 0 ? "Default location." : name);
	node->description = description;
	node->textureWall = textureWall;
	if (modelExists(buf)) 
		node->poi_model.Load(buf);
	else
		node->poi_model.Load("./Models/poi/default/Model.3ds");
	node->poi_model.pos.x = node->coords->getX();
	node->poi_model.pos.y = node->coords->getY();
	node->poi_model.pos.z = node->coords->getZ();
	if (numNodes_++ == 0) {
		player.coords = CartCoordinate(x, y, z);
		player.state.on_obj = NODE;
		player.state.on_obj_id = player.state.travelling_from = id;
	}
}

/* Returns the reference of a Node with 
 * the provided id.
 */
No* Graph::getNode(int id) {
	No* ptr = NULL;
	for (int i = 0; i < numNodes_; i++){
		if (nodes_[i].id == id) {
			ptr = &nodes_[i];
		}
	}
	return ptr;
}

Arco* Graph::getWay(int id) {
	Arco* ptr = NULL;
	if (id < MAX_WAYS) {
		ptr = &ways_[id];
	}
	return ptr;
}

float Graph::getDistance(float* from, float* to) {
	return (sqrt(pow(to[X] - from[X], 2) + pow(to[Y] - from[Y], 2) + pow(to[Z] - from[Z], 2)));
}

Player* Graph::getPlayer() {
	return &player;
}

Camera* Graph::getCamera() {
	return &camera;
}

void Graph::updateCamera() {
	float v[3], center[3], eye[3];
	player.coords.toArray(v);

	switch (camera.type) {
	case FIRST_PERSON:
		center[X] = v[X] + cos(camera.dir_long); // *cos(camera.dir_lat);
		center[Y] = v[Y] + sin(camera.dir_long); // *cos(camera.dir_lat);
		center[Z] = v[Z] + MODEL_SIZE * 150;
		eye[X] = v[X];
		eye[Y] = v[Y];
		eye[Z] = center[Z];
		break;
	case THIRD_PERSON:
		center[X] = v[X];
		center[Y] = v[Y];
		center[Z] = v[Z] + MODEL_SIZE * 50 * 1.5;
		eye[X] = v[X] - camera.player_dist * cos(camera.dir_long); //* cos(camera.dir_lat);
		eye[Y] = v[Y] - camera.player_dist * sin(camera.dir_long); //* cos(camera.dir_lat);
		eye[Z] = center[Z] + MODEL_SIZE * 50;
	}

	gluLookAt(eye[X], eye[Y], eye[Z], center[X], center[Y], center[Z], 0, 0, 1);
}

void Graph::updatePerspective() {
	gluPerspective(camera.fov, (GLfloat)glutGet(GLUT_WINDOW_WIDTH) / glutGet(GLUT_WINDOW_HEIGHT), 0.1f, camera.view_dist);
}

void Graph::changeModel() {
	switch (player.state.current_model) {
	case HOMER_MODEL:
		player.state.current_model = CAR_MODEL;
		player.velocity *= 7.0f;
		break;
	case CAR_MODEL:
		player.state.current_model = HOMER_MODEL;
		player.velocity /= 7.0f;
		break;
	}
}

void Graph::changeCamera() {
	switch (camera.type) {
	case FIRST_PERSON:
		camera.type = THIRD_PERSON;
		break;
	case THIRD_PERSON:
		camera.type = FIRST_PERSON;
		break;
	}
}

void Graph::setCameraType(CameraType type) {
	camera.type = type;
}

void Graph::incOrientation(float inc) {
	player.orientation += inc;
	camera.dir_long += inc;
}
/* Creates and stores a Way structure containing the addresses 
 * of the origin/destination Nodes, a numeric weight and a 
 * numeric width.
 */
void Graph::addWay(int id1, int id2, std::string name, float weight, float width, glTexture texture,
	glTexture textureWall, glTexture sidewalk, bool belongsVisit, bool isFoot) {
	if (numWays_ == MAX_WAYS)
		return;
	Arco* arco = &ways_[numWays_++];
	arco->name = (sizeof name == 0 ? "Default location." : name);
	arco->ni = getNode(id1), arco->nf = getNode(id2);
	arco->belongsVisit = belongsVisit;
	arco->isFoot = isFoot;
	GLfloat vi[3], vf[3], vfinal[3];
	arco->ni->coords->toArray(vi);
	arco->nf->coords->toArray(vf);
	vfinal[X] = vf[X] - vi[X];
	vfinal[Y] = vf[Y] - vi[Y];
	vfinal[Z] = vf[Z] - vi[Z];

	arco->weight = (weight < 1.0f ? 1.0f : weight);
	arco->width = (width < 1.0f ? 1.0f : width);
	arco->projection = sqrt(pow(vfinal[X], 2) + pow(vfinal[Y], 2)) - (arco->ni->s) - (arco->nf->s);
	arco->length = sqrt(pow(arco->projection, 2) + pow(vfinal[Z], 2));
	arco->steepness = degrees(atan2(vfinal[Z], arco->projection));
	arco->orientation = normalize(vfinal);
	arco->textureGround = texture;
	arco->textureWall = textureWall;
	arco->textureSideWalk = sidewalk;
}

void Graph::renderGround(float* color, bool centerPlayer) {
	glColor4f(color[0], color[1], color[2], color[3]);
	glPushMatrix();
	if(centerPlayer)
		glTranslatef(player.coords.getX(), player.coords.getY(), -5.0f);
	glBegin(GL_QUADS);
	glNormal3f(0, 0, 1);
	for (int i = -500; i < 500; i += 10)
		for (int j = -150; j < 150; j += 10) {
			glVertex2f(i, j);
			glVertex2f(i + 10, j);
			glVertex2f(i + 10, j + 10);
			glVertex2f(i, j + 10);
		}
	glEnd();
	glPopMatrix();
}

void Graph::renderPlayer(StudioModel* model, bool minimap) {
	float v[3], size = MODEL_SIZE, translate = MODEL_SIZE * 30;
	player.coords.toArray(v);
	glEnable(GL_TEXTURE_2D);
	glPushMatrix();
		glTranslatef(v[X], v[Y], v[Z]);
		glRotatef(degrees(player.orientation), 0.0f, 0.0f, 1.0f);
		if (player.state.current_model == CAR_MODEL) {
			glRotatef(-90, 0.0f, 0.0f, 1.0f);
			if (player.state.on_obj == WAY) {
				Arco* way = getWay(player.state.on_obj_id);
				float steepness = (way->ni->id != player.state.travelling_from ? -way->steepness : way->steepness);
				glRotatef(steepness, 1.0f, 0.0f, 0.0f);
			}
			size *= 0.75f;
			translate = 0;
		}
		glTranslatef(0.0f, 0.0f, translate);
		if (!minimap)
			glScalef(size, size, size);
		else
			glScalef(size * 5, size * 5, size * 5);
		mdlviewer_display(*model);
	glPopMatrix();
	glDisable(GL_TEXTURE_2D);
	glutPostRedisplay();
}

void Graph::renderNodes() {
	glColor3f(0.0f, 0.0f, 0.8f);
	float step = 360 / 60;
	No* node = nodes_;
	for (int i = 0; i < numNodes_; i++) {
		float vn[3], vp[3];
		node->coords->toArray(vn);
		player.coords.toArray(vp);
		float distance = getDistance(vp, vn);
		if (distance >= camera.view_dist + node->s) {
			node++;
			continue;
		}
		// Save this object's type.
		glPushName(NODE);
		// Save this object's id.
		glPushName(node->id);
		glPushMatrix();
		glEnable(GL_TEXTURE_2D);
		glBindTexture(GL_TEXTURE_2D, node->textureWall.TextureID);
		glBegin(GL_POLYGON);
		for (float i = 0; i < 360; i += step) {
			float r[3];
			r[X] = vn[X] + cos(radians(i)) * node->radius;
			r[Y] = vn[Y] + sin(radians(i)) * node->radius;
			r[Z] = vn[Z];
			glTexCoord3fv(r);
			glVertex3fv(r);
		}
		glEnable(GL_TEXTURE_2D);
		glBindTexture(GL_TEXTURE_2D, NULL);
		glEnd();
		if (distance <= node->s) {
			glEnable(GL_TEXTURE_2D);
			node->poi_model.Draw();
			glDisable(GL_TEXTURE_2D);
		}
		glPopMatrix();
		glPopName();
		glPopName();
		node++;
	}
}

void Graph::renderNodeConnection(No* node, float orientation, float width, float length) {
	glEnable(GL_TEXTURE_2D);
	glBindTexture(GL_TEXTURE_2D, node->textureWall.TextureID);
	glColor3f(0.0f, 0.0f, 0.8f);
	// Save this object's type.
	glPushName(NODE);
	// Save this object's id.
	glPushName(node->id);
	glPushMatrix();
		glTranslatef(node->coords->getX(), node->coords->getY(), node->coords->getZ()-0.01f);
		glRotatef(orientation, 0.0f, 0.0f, 1.0f);
		glPushMatrix();
			glTranslatef(0.0f, width / -2.0f , 0.0f);
			glBegin(GL_QUADS);
			glVertex3f(0.0f, 0.0f, 0.0f);
			glVertex3f(0.0f, width, 0.0f);
			glVertex3f(length, width, 0.0f);
			glVertex3f(length, 0.0f, 0.0f);
			glEnd();
		glPopMatrix();
	glPopMatrix();
	glPopName();
	glPopName();
	glEnable(GL_TEXTURE_2D);
	glBindTexture(GL_TEXTURE_2D, NULL);
}

void Graph::renderWays() {
	Arco* way = ways_;
	for (int i = 0; i < numWays_; i++){
		// Selective Way rendering.
		float vi[3], vf[3], vp[3], res[3];
		way->ni->coords->toArray(vi);
		way->nf->coords->toArray(vf);
		player.coords.toArray(vp);
		// Distance from player's location to the center of node ni.
		float dist_ni = getDistance(vp, vi);
		// Distance from player's location to the center of node nf.
		float dist_nf = getDistance(vp, vf);
		// res[] -> This way's midpoint coordinates.
		res[X] = ((vf[X] - vi[X]) / 2.0f) + vi[X];
		res[Y] = ((vf[Y] - vi[Y]) / 2.0f) + vi[Y];
		res[Z] = ((vf[Z] - vi[Z]) / 2.0f) + vi[Z];
		// Distance from player's location to this way's midpoint.
		float dist_center = getDistance(vp, res);
		// Checking whether this ways's nodes aren't within
		// the player's view distance and if the player isn't
		// currently in this way.
		// In case this statement is true, this way and 
		// it's node connections won't be rendered.
		if (dist_ni >= camera.view_dist * way->ni->s 
			&& dist_nf >= camera.view_dist * way->nf->s
			&& dist_center > way->length / 2.0f) {
			way++;
			continue;
		}
		renderNodeConnection(way->	ni, way->orientation, way->width, way->ni->s);
		renderNodeConnection(way->nf, way->orientation + 180, way->width, way->ni->s);

		double ratio = way->length * 100.0f / (double) way->textureGround.Height;

		glEnable(GL_TEXTURE_2D);
		glBindTexture(GL_TEXTURE_2D, way->textureGround.TextureID);
		glPushName(WAY);
		glPushName(i);
		glPushMatrix();
			glTranslatef(way->ni->coords->getX(), way->ni->coords->getY(), way->ni->coords->getZ());
			glRotatef(way->orientation, 0.0f, 0.0f, 1.0f);
			glPushMatrix();
				glTranslatef(way->ni->s, 0.0f, 0.0f);
				glRotatef(-way->steepness, 0.0f, 1.0f, 0.0f);
				glPushMatrix();
					glTranslatef(0.0f, way->width / -2.0f, 0.0);
					glBegin(GL_QUADS);
					glNormal3f(0.0f, 0.0f, 1.0f);
					glTexCoord2f(0.0f, 0.0f);
					glVertex3f(0.0f, 0.0f, 0.0f);
					glTexCoord2f(0.0f, ratio);
					glVertex3f(way->length, 0.0f, 0.0f);
					glTexCoord2f(1.0f, ratio);
					glVertex3f(way->length, way->width, 0.0f);
					glTexCoord2f(1.0f, 0.0f);
					glVertex3f(0.0f, way->width, 0.0f);	
					glEnd();
				glPopMatrix();
			glPopMatrix();
		glPopMatrix();
		glPopName();
		glPopName();
		glBindTexture(GL_TEXTURE_2D, NULL);
		glDisable(GL_TEXTURE_2D);

		// ----
		glEnable(GL_TEXTURE_2D);
		glBindTexture(GL_TEXTURE_2D, way->textureWall.TextureID);

		glPushName(WAY);
		glPushName(i);
		glPushMatrix();
			glTranslatef(way->ni->coords->getX(), way->ni->coords->getY(), way->ni->coords->getZ());
			glRotatef(way->orientation, 0.0f, 0.0f, 1.0f);
			glPushMatrix();
				glTranslatef(way->ni->s, 0.0f, 0.0f);
				glRotatef(-way->steepness, 0.0f, 1.0f, 0.0f);
				// zzzz
				glRotatef(90.0, 1.0f, 0.0f, 0.0f);
				glTranslatef(0.0f, way->width / 2, way->width/2);
				glPushMatrix();
					glTranslatef(0.0f, way->width / -2.0f, 0.0);
					glBegin(GL_QUADS);
					glNormal3f(0.0f, 0.0f, 1.0f);
					glTexCoord2f(0.0f, 0.0f);
					glVertex3f(0.0f, 0.0f, 0.0f);
					glTexCoord2f(0.0f, ratio);
					glVertex3f(way->length, 0.0f, 0.0f);
					glTexCoord2f(1.0f, ratio);
					glVertex3f(way->length, 1.0f, 0.0f);
					glTexCoord2f(1.0f, 0.0f);
					glVertex3f(0.0f, 1.0f, 0.0f);
					glEnd();
				glPopMatrix();
			glPopMatrix();
		glPopMatrix();

		glPushMatrix();
			glTranslatef(way->ni->coords->getX(), way->ni->coords->getY(), way->ni->coords->getZ());
			glRotatef(way->orientation, 0.0f, 0.0f, 1.0f);
			glPushMatrix();
				glTranslatef(way->ni->s, 0.0f, 0.0f);
				glRotatef(-way->steepness, 0.0f, 1.0f, 0.0f);
				// zzzz
				glRotatef(90.0, 1.0f, 0.0f, 0.0f);
				glTranslatef(0.0f, way->width / 2, -way->width / 2);
				glPushMatrix();
					glTranslatef(0.0f, way->width / -2.0f, 0.0);
					glBegin(GL_QUADS);
					glNormal3f(0.0f, 0.0f, 1.0f);
					glTexCoord2f(0.0f, 0.0f);
					glVertex3f(0.0f, 0.0f, 0.0f);
					glTexCoord2f(0.0f, ratio);
					glVertex3f(way->length, 0.0f, 0.0f);
					glTexCoord2f(1.0f, ratio);
					glVertex3f(way->length, 1.0f, 0.0f);
					glTexCoord2f(1.0f, 0.0f);
					glVertex3f(0.0f, 1.0f, 0.0f);
					glEnd();
				glPopMatrix();
			glPopMatrix();
		glPopMatrix();
		glPopName();
		glPopName();
		glBindTexture(GL_TEXTURE_2D, NULL);
		glDisable(GL_TEXTURE_2D);

		if (way->belongsVisit) {
			
			if (way->isFoot) {
				glColor4f(1.0, 1.0, 1.0, 0.2f);
			}
			else {
				glColor4f(1.0, 0.0, 0.0, 0.2f);
			}

			glEnable(GL_BLEND); 
			glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
			/*glEnable(GL_TEXTURE_2D);
			glBindTexture(GL_TEXTURE_2D, way->texture.TextureID);*/
			glPushName(WAY);
			glPushName(i);
			glPushMatrix();
			glTranslatef(way->ni->coords->getX(), way->ni->coords->getY(), way->ni->coords->getZ());
			glRotatef(way->orientation, 0.0f, 0.0f, 1.0f);
			glPushMatrix();
			glTranslatef(way->ni->s, 0.0f, 0.0f);
			glRotatef(-way->steepness, 0.0f, 1.0f, 0.0f);
			glPushMatrix();
			glTranslatef(0.0f, way->width / -2.0f, 0.0);
			glBegin(GL_QUADS);
			glNormal3f(0.0f, 0.0f, 1.0f);
			glTexCoord2f(0.0f, 0.0f);
			glVertex3f(0.0f, 0.0f + way->width/2 - 0.1f, 0.01f);
			glTexCoord2f(0.0f, ratio);
			glVertex3f(way->length, 0.0f + way->width/2 - 0.1f, 0.01f);
			glTexCoord2f(1.0f, ratio);
			glVertex3f(way->length, way->width/2 + 0.1f, 0.01f);
			glTexCoord2f(1.0f, 0.0f);
			glVertex3f(0.0f, way->width/2 + 0.01f, 0.01f);
			glEnd();
			glPopMatrix();
			glPopMatrix();
			glPopMatrix();
			glPopName();
			glPopName();
			glBindTexture(GL_TEXTURE_2D, NULL);
			glDisable(GL_TEXTURE_2D);
			glDisable(GL_BLEND);
		}



		//glBegin(GL_QUADS);
		////glNormal3dv(cross);
		//glVertex3f(xi, yi, zi);
		//glVertex3f(xf, yf, zf + 0);
		//glVertex3f(xf, yf, zf + 1);
		//glVertex3f(xi, yi, zi + 1);
		//glEnd();
		/*glBegin(GL_QUADS);*/
		//glNormal3dv(cross);
		//glColor3f(0.5, 0.5, 0.5);
		//glVertex3f(way->ni->coords->getX() - way->width / 2, way->ni->coords->getY(), way->ni->coords->getZ()  -2);
		//glVertex3f(way->nf->coords->getX() - way->width / 2, way->nf->coords->getY(), way->nf->coords->getZ() -2);
		//glVertex3f(way->nf->coords->getX() - way->width / 2, way->nf->coords->getY(), way->nf->coords->getZ() + 2);
		//glVertex3f(way->ni->coords->getX() - way->width / 2, way->ni->coords->getY(), way->ni->coords->getZ() + 2);
		//glEnd();



		way++;
	}
}

void Graph::renderSkyBox(glTexture* texture) {
	if (NULL == texture) return;
	glPushMatrix();
	glTranslatef(player.coords.getX(), player.coords.getY(), player.coords.getZ());

	glEnable(GL_TEXTURE_2D);
	glBindTexture(GL_TEXTURE_2D, texture->TextureID);
	GLUquadricObj* quad = gluNewQuadric();
	gluQuadricDrawStyle(quad, GLU_FILL);
	gluQuadricNormals(quad, GLU_SMOOTH);
	gluQuadricTexture(quad, GL_TRUE);
	gluSphere(quad, camera.view_dist-10, 20, 50);
	gluDeleteQuadric(quad);
	glDisable(GL_TEXTURE_2D);
	glPopMatrix();
}
