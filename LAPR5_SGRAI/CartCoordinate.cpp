#include "CartCoordinate.h"

CartCoordinate::CartCoordinate()
{
}

float CartCoordinate::getX() {
	return x_;
}

float CartCoordinate::getY() {
	return y_;
}

float CartCoordinate::getZ() {
	return z_;
}

void CartCoordinate::setX(const float x) {
	x_ = x;
}

void CartCoordinate::setY(const float y) {
	y_ = y;
}

void CartCoordinate::setZ(const float z) {
	z_ = z;
}

void CartCoordinate::toArray(float* v) {
	v[0] = x_;
	v[1] = y_;
	v[2] = z_;
}