#include <stdio.h>
#include <glut.h>

#include "Graph.h"
#include "Importer.h"

#define NUM_TEXTURES 6
#define TX_ROAD 0
#define TX_WALL 1
#define TX_ROAD_VISIT 2
#define TX_SKY 3
#define TX_GRASS 4
#define TX_SIDEWALK 5

typedef struct {
	GLboolean up;
	GLboolean down;
	GLboolean left;
	GLboolean right;
} Input;

Input input;


glTexture textures[NUM_TEXTURES];
StudioModel models[NUM_MODELS];
Graph* graph;

void reshape(int w, int h) {

	glViewport(0, 0, w, h);

	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	graph->updatePerspective();

	glMatrixMode(GL_MODELVIEW);
}


void drawMinimap() {
	const int offset = glutGet(GLUT_WINDOW_HEIGHT) / 4.0f;
	float v[3];
	Player* p = NULL;
	p = graph->getPlayer();
	p->coords.toArray(v);
	glViewport(glutGet(GLUT_WINDOW_WIDTH) - offset, 0, offset, offset);

	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	glOrtho(-offset * 0.08f, offset * 0.08f, -offset * 0.08f, offset * 0.08f, -500.0f, 500.0f);

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	
	gluLookAt(v[X], v[Y], 0.0f, v[X], v[Y], -1.0f, 0.0f, 1.0f, 0.0f);
	glEnable(GL_BLEND);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
	float color[] = { 0.2f, 0.2f, 0.2f, 0.8f };
	glPushMatrix();
		graph->renderGround(color);
		graph->renderNodes();
		graph->renderWays();
		graph->renderPlayer(&models[p->state.current_model], true);
	glPopMatrix();
	glDisable(GL_BLEND);
}



GLuint* picking(float x, float y, float z, GLdouble* nz = NULL) {
	double zmin = z - 1.0f, zmax = z + 1.0f;
	int i, n;
	GLuint buffer[100], *ptr1 = NULL, *ptr2 = NULL;

	glSelectBuffer(100, buffer);
	glRenderMode(GL_SELECT);
	glInitNames();

	glMatrixMode(GL_PROJECTION);
	glPushMatrix();
	glLoadIdentity();
	GLint vport[4];
	glGetIntegerv(GL_VIEWPORT, vport);
	gluPickMatrix(glutGet(GLUT_WINDOW_WIDTH) / 2.0, glutGet(GLUT_WINDOW_HEIGHT) / 2.0f, 15.0f, 15.0f, vport);
	glOrtho(x - 0.001, x + 0.001, y - 0.001, y + 0.001, -zmax, -zmin);

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glPushMatrix();
		graph->renderNodes();
		graph->renderWays();
	glPopMatrix();
	gluLookAt(x, y, zmax + 0.1, x, y, zmax, 0.0f, 1.0f, 0.0f);

	double tmp = zmax;
	n = glRenderMode(GL_RENDER);
	if (n > 0)
	{
		ptr1 = buffer;
		for (i = 0; i < n; i++)
		{
			if (tmp >(double) ptr1[1] / UINT_MAX) {
				tmp = (double)ptr1[1] / UINT_MAX;
				ptr2 = ptr1;
			}

			ptr1 += 3 + ptr1[0]; // ptr[0] contem o n�mero de nomes (normalmente 1); 3 corresponde a numnomes, zmin e zmax
		}
	}

	if (nz != NULL) 
		*nz = zmax - (tmp * (zmax - zmin));

	glMatrixMode(GL_PROJECTION); //rep�e matriz projec��o
	glPopMatrix();
	glMatrixMode(GL_MODELVIEW);
	return ptr2;
}

void showInformation() {
	const int buf_size = 1024;
	float v[3];
	graph->getPlayer()->coords.toArray(v);
	GLuint* ptr = picking(v[X], v[Y], v[Z]);
	char name[buf_size], details[buf_size];
	strcpy_s(name, buf_size, "Location: ");
	strcpy_s(details, buf_size, "Details: ");
	if (NULL != ptr) {
		switch (ptr[3]) {
			case ObjectType::NODE: {
				No* n = graph->getNode(ptr[4]);
				strcat_s(name, buf_size, &n->name.front());
				strcat_s(details, buf_size, &n->description.front());
				strcat_s(details, buf_size, &string(".").front());
				break;
			}
			case ObjectType::WAY: {
				Arco* a = graph->getWay(ptr[4]);
				strcat_s(name, buf_size, &a->name.front());
				stringstream ss;
				ss << "This road is " << a->weight << " meters long.";
				strcat_s(details, buf_size, &ss.str().front());
				break;
			}
		}
	}

	float width = glutGet(GLUT_WINDOW_WIDTH), height = glutGet(GLUT_WINDOW_HEIGHT), offset = 50.0f;
	glViewport(0.0f, height - offset, width, offset);

	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	gluOrtho2D(0, width * 0.1, -offset * 0.1, 0);

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glPushMatrix();
		glDisable(GL_DEPTH_TEST);
		float color[] = { 0.3f, 0.3f, 0.3f, 0.7f };
		glEnable(GL_BLEND);
		glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
		graph->renderGround(color, false);
		glDisable(GL_BLEND);
		glColor3f(0.9f, 0.9f, 0.9f);
		glRasterPos2i(1.0f, -2.0f);
		for (int i = 0; i < strlen(name); i++) {
			glutBitmapCharacter(GLUT_BITMAP_9_BY_15, name[i]);
		}
		glRasterPos2i(1.0f, -3.0f);
		for (int i = 0; i < strlen(details); i++) {
			glutBitmapCharacter(GLUT_BITMAP_8_BY_13, details[i]);
		}
		glRasterPos2i(1.0f, -4.0f);
		for (int i = 0; i < graph->getVisitDescription().size(); i++) {
			glutBitmapCharacter(GLUT_BITMAP_8_BY_13, graph->getVisitDescription().c_str()[i]);
		}
		glEnable(GL_DEPTH_TEST);
	glPopMatrix();
}

void setProjection(int x, int y, GLboolean picking) {
	glLoadIdentity();
	if (picking) { // se est� no modo picking, l� viewport e define zona de picking
		GLint vport[4];
		glGetIntegerv(GL_VIEWPORT, vport);
		gluPickMatrix(x, glutGet(GLUT_WINDOW_HEIGHT) - y, 4, 4, vport); // Inverte o y do rato para corresponder � jana
	}
	gluPerspective(60, (GLfloat)glutGet(GLUT_WINDOW_WIDTH) / glutGet(GLUT_WINDOW_HEIGHT), 1, 500);
}

//void motionZoom(int x, int y) {
//#define ZOOM_SCALE	0.5
//	state.camera.dist -= (state.yMouse - y) * ZOOM_SCALE;
//	if (state.camera.dist<5)
//		state.camera.dist = 5;
//	else
//		if (state.camera.dist>200)
//			state.camera.dist = 200;
//	state.yMouse = y;
//	glutPostRedisplay();
//}

//void motionRotate(int x, int y) {
//#define DRAG_SCALE	0.01
//	double lim = M_PI / 2 - 0.1;
//	state.camera.dir_long += (state.xMouse - x) * DRAG_SCALE;
//	state.camera.dir_lat -= (state.yMouse - y) * DRAG_SCALE * 0.5;
//	if (state.camera.dir_lat > lim)
//		state.camera.dir_lat = lim;
//	else
//		if (state.camera.dir_lat < -lim)
//			state.camera.dir_lat = -lim;
//	state.xMouse = x;
//	state.yMouse = y;
//	glutPostRedisplay();
//}

//void mouseFunc(int btn, int press, int x, int y) {
//	switch (btn) {
//		case GLUT_RIGHT_BUTTON:
//			if (press == GLUT_DOWN) {
//				state.xMouse = x;
//				state.yMouse = y;
//				if (glutGetModifiers() & GLUT_ACTIVE_CTRL)
//					glutMotionFunc(motionZoom);
//				else
//					glutMotionFunc(motionRotate);
//			}
//			else {
//				glutMotionFunc(NULL);
//			}
//			break;
//		case GLUT_LEFT_BUTTON:
//			if (press == GLUT_DOWN) {
//				std::cout << "Clicked with mouse in " << "(" << x << "," << y << ")" << endl;
//			}
//	}
//}

void Timer(int value) {
	float v[3];
	GLdouble nx, ny, nz;
	Camera* cam = graph->getCamera();
	Player* p = graph->getPlayer();
	p->coords.toArray(v);
	bool moving = GL_FALSE;
	float orientation = graph->getPlayer()->orientation;
	GLuint curr = glutGet(GLUT_ELAPSED_TIME);
	glutTimerFunc(16, Timer, 0);

	if (input.up || input.down) {
		float sign = (input.up ? 1 : -1);
		nx = v[X] + cos(-orientation) * p->velocity * sign;
		ny = v[Y] + sin(orientation) * p->velocity * sign;
		if (picking(nx, ny, v[Z], &nz)) {
			p->coords.setX(nx);
			p->coords.setY(ny);
			p->coords.setZ(nz);
			moving = GL_TRUE;
		}
	}
	if (input.left) {
		p->orientation += radians(MODEL_ROTATION);
		cam->dir_long += radians(MODEL_ROTATION);
	}
	if (input.right) {
		p->orientation += -radians(MODEL_ROTATION);
		cam->dir_long += -radians(MODEL_ROTATION);
	}

	if (models[HOMER_MODEL].GetSequence() != 20) {
		if (moving && models[HOMER_MODEL].GetSequence() != 3)
			models[HOMER_MODEL].SetSequence(3);
		else if (!moving && models[HOMER_MODEL].GetSequence() != 0)
			models[HOMER_MODEL].SetSequence(0);
	}

	glutPostRedisplay();
}

void setLight(GLfloat* position = NULL)
{
	GLfloat light_pos[4] = { 0.0f, 0.0f, 50.0f, 1.0 };
	GLfloat light_ambient[] = { 0.5f, 0.5f, 0.5f, 1.0f };
	GLfloat light_diffuse[] = { 0.8f, 0.8f, 0.8f, 1.0f };
	GLfloat light_specular[] = { 0.5f, 0.5f, 0.5f, 1.0f };
	glEnable(GL_LIGHT0);
	glLightfv(GL_LIGHT0, GL_AMBIENT, light_ambient);
	glLightfv(GL_LIGHT0, GL_DIFFUSE, light_diffuse);
	glLightfv(GL_LIGHT0, GL_SPECULAR, light_specular);
	if (position == NULL)
		glLightfv(GL_LIGHT0, GL_POSITION, light_pos);
	else
		glLightfv(GL_LIGHT0, GL_POSITION, position);

	glLightModeli(GL_LIGHT_MODEL_LOCAL_VIEWER, 1);
}

void changeTooltip() {
	graph->setShowTooltip(!graph->getShowTooltip());
}

void setMaterial()
{
	GLfloat mat_specular[] = { 0.8f, 0.8f, 0.8f, 1.0f };
	GLfloat mat_shininess = 104;
	glEnable(GL_COLOR_MATERIAL);
	glColorMaterial(GL_FRONT, GL_AMBIENT_AND_DIFFUSE);
	glMaterialfv(GL_FRONT, GL_SPECULAR, mat_specular);
	glMaterialf(GL_FRONT, GL_SHININESS, mat_shininess);
}

void loadTextures() {
	TextureLoader* tl = new TextureLoader();

	tl->LoadTextureFromDisk("Textures/road-texture.JPG", &textures[TX_ROAD]);
	tl->LoadTextureFromDisk("Textures/wall.JPG", &textures[TX_WALL]);
	tl->LoadTextureFromDisk("Textures/road-texture-visit.jpg", &textures[TX_ROAD_VISIT]);
	tl->LoadTextureFromDisk("Textures/sky.jpg", &textures[TX_SKY]);
	tl->LoadTextureFromDisk("Textures/grass.jpg", &textures[TX_GRASS]);
	tl->LoadTextureFromDisk("Textures/sidewalk.jpg", &textures[TX_SIDEWALK]);
}

void myInit()
{
	GLfloat sceneLight[] = { 0.7, 0.7, 0.7, 0.0f };

	glClearColor(0.4f, 0.6f, 1.0f, 0.0f);

	glShadeModel(GL_SMOOTH); /*enable smooth shading */
	glEnable(GL_POINT_SMOOTH);
	glEnable(GL_LINE_SMOOTH);
	glEnable(GL_POLYGON_SMOOTH);
	glEnable(GL_LIGHTING); /* enable lighting */
	glEnable(GL_DEPTH_TEST); /* enable z buffer */
	glEnable(GL_NORMALIZE);

	glDepthFunc(GL_LESS);

	glLightModelfv(GL_LIGHT_MODEL_AMBIENT, sceneLight);
	glLightModeli(GL_LIGHT_MODEL_LOCAL_VIEWER, 1);
	glLightModeli(GL_LIGHT_MODEL_TWO_SIDE, GL_TRUE);

	mdlviewer_init("./Models/player/homer.mdl", models[HOMER_MODEL]);
	mdlviewer_init("./Models/player/car.mdl", models[CAR_MODEL]);

	input.up = input.down = input.left = input.right = GL_FALSE;
	loadTextures();
}

void createGraph(vector<Node> * node_list, vector<Way> * way_list, vector<Visit> * visit_list) {
	cout << endl << "Graph LAPR5_SGRAI" << endl;
	graph = new Graph();

	if (!node_list->empty()) {
		for (std::vector<Node>::iterator it = node_list->begin();
			it != node_list->end(); ++it) {
			
			cout<< "Graph::addNode(" << it->getName()<<") ." << endl;
			graph->addNode(it->getId(),it->getName(), it->getDescription(), it->getCoordinatesCart().getX(),
				it->getCoordinatesCart().getY(),
				it->getAltitude() / 3,
				10.0f, textures[TX_GRASS]);
		}

		graph->listNodes();
	}

	if (!way_list->empty()) {
		int countAdd = 0;

		for (std::vector<Way>::iterator it = way_list->begin();
			it != way_list->end(); ++it) {

			cout << "Graph::addWay(POI " << it->getPOI1ID() 
				<< " and POI " << it->getPOI2ID() << " distanced " << it->getDistance() << ")   " ;


			if (!visit_list->empty()) {
				

				bool flagBelongsToVisit = false;
				for (std::vector<Visit>::iterator it2 = visit_list->begin();
					it2 != visit_list->end(); ++it2) {

					if(((it->getPOI1ID() == it2->getId1()) && (it->getPOI2ID() == it2->getId2()))
						|| ((it->getPOI1ID() == it2->getId2()) && (it->getPOI2ID() == it2->getId1()))) {

						flagBelongsToVisit = true;
						
						//&graph->getNode(ptr[4])->name.front()

						graph->addWay(it->getPOI1ID(), it->getPOI2ID(),
							Way::generateName(graph->getNode(it->getPOI1ID())->name, graph->getNode(it->getPOI2ID())->name), it->getDistance(), 1.7f, textures[TX_ROAD_VISIT],
							textures[TX_WALL], textures[TX_SIDEWALK], true, it2->isFoot());

						if(countAdd == 0) graph->player.coords = CartCoordinate(graph->getNode(it->getPOI1ID())->coords->getX(),
							graph->getNode(it->getPOI1ID())->coords->getY(), graph->getNode(it->getPOI1ID())->coords->getZ());

						

						

						++countAdd;
						break;
					}

					if(!flagBelongsToVisit) graph->addWay(it->getPOI1ID(), it->getPOI2ID(),
						Way::generateName(graph->getNode(it->getPOI1ID())->name, graph->getNode(it->getPOI2ID())->name), it->getDistance(), 1.7f, textures[TX_ROAD],
						textures[TX_WALL], textures[TX_SIDEWALK], false, false);

					++countAdd;
				}
			}
			else {
				graph->addWay(it->getPOI1ID(), it->getPOI2ID(),
					Way::generateName(graph->getNode(it->getPOI1ID())->name, graph->getNode(it->getPOI2ID())->name), it->getDistance(), 1.7f, textures[TX_ROAD],
					textures[TX_WALL], textures[TX_SIDEWALK], false, false);
				++countAdd;
			}


			
		}
				
	}

	if (!visit_list->empty()) {
		std::string description_visit = string("Route: ");
		stringstream ss; ss << description_visit;
		
		int i = 0;
		
		for (std::vector<Visit>::iterator it = visit_list->begin();
			it != visit_list->end(); ++it) {
			++i;
			cout << "Graph::addVisit(POI " << it->getId1() << "). i = "<<i  << endl;
			ss << graph->getNode(it->getId1())->name ;
			if (i==visit_list->size()) {
				ss << ".";
			}
			else {
				ss << ", ";
				
			}
			
		}
		graph->setVisitDescription(ss.str());
	}

	cout << graph->getVisitDescription();

	//getchar();
}

void createGraph() {
	// ----------------------- addNode()
	//graph->addNode(0, "POI 1", 0.0f, 0.0f, 0.0f, 4.0f);
	//graph->addNode(1, "POI 2", 20.0f, 0.0f, 0.0f, 4.0f);
	//graph->addNode(2, "POI 3", 0.0f, 20.0f, 0.0f, 4.0f);
	//graph->addNode(3, "POI 4", 40.0f, 40.0f, 5.0f, 4.0f);
	// ----------------------- addNode()

	//// ----------------------- addWay()
	//graph->addWay(0, 1, "Rua 1", 5.0f, 2.0f, textures[TX_ROAD], textures[TX_WALL], false, false);
	//graph->addWay(0, 2, "Rua 2", 5.0f, 1.0f, textures[TX_ROAD], textures[TX_WALL], false, false);
	//graph->addWay(1, 3, "Rua 3", 5.0f, 2.0f, textures[TX_ROAD], textures[TX_WALL], false, false);
	//graph->addWay(2, 3, "Rua 4", 5.0f, 2.0f, textures[TX_ROAD], textures[TX_WALL], false, false);
	// ----------------------- addWay()
}

void renderScene(void)
{
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	Player* p = graph->getPlayer();
	float vp[3];
	p->coords.toArray(vp);
	GLuint* pick = NULL;
	pick = picking(vp[X], vp[Y], vp[Z]);
	if (NULL != pick) {
		if (NODE == ObjectType(pick[3]) && p->state.travelling) {
			p->state.travelling = false;
			p->state.on_obj = NODE;
			p->state.on_obj_id = int(pick[4]);
			p->state.travelling_from = int(pick[4]);
		}
		if (WAY == ObjectType(pick[3]) && !p->state.travelling) {
			p->state.travelling = true;
			p->state.on_obj = WAY;
			p->state.on_obj_id = int(pick[4]);
		}
	}
	glLoadIdentity();

	float ground_color[] = { 0.5f, 0.5f, 0.5f, 1.0f };
	graph->updateCamera();
	glPushMatrix();
		graph->renderGround(ground_color);
		graph->renderPlayer(&models[p->state.current_model]);
		graph->renderNodes();
		graph->renderWays();
		graph->renderSkyBox(&textures[TX_SKY]);
	glPopMatrix();

	
	glDisable(GL_LIGHTING);
	showInformation();
	drawMinimap();
	glEnable(GL_LIGHTING);

	reshape(glutGet(GLUT_WINDOW_WIDTH), glutGet(GLUT_WINDOW_HEIGHT));

	glFlush();
	glutSwapBuffers();
}

void keyboardFunc(unsigned char key, int x, int y) {
	switch (key) {
	case 'w':
		input.up = GL_TRUE;
		break;
	case 's':
		input.down = GL_TRUE;
		break;
	case 'a':
		input.left = GL_TRUE;
		break;
	case 'd':
		input.right = GL_TRUE;
		break;
	case 'c':
		graph->changeModel();
		break;
	case 'v':
		graph->changeCamera();
		break;
	case '.':
		changeTooltip();
		break;
	}
}

void keyboardUpFunc(unsigned char key, int x, int y) {
	switch (key) {
	case 'w':
		input.up = GL_FALSE;
		break;
	case 's':
		input.down = GL_FALSE;
		break;
	case 'a':
		input.left = GL_FALSE;
		break;
	case 'd':
		input.right = GL_FALSE;
		break;
	}
}

int main(int argc, char** argv) {

	// ----------------------------------------------------- IMPORTER
	cout << fixed; cout << setprecision(0);
	Importer mapDataImporter = Importer(__FLAG_LOADER_API_REST);
	vector<Node> * node_list = new vector<Node>();
	mapDataImporter.loadData(node_list);
	cout << "Loaded  " << node_list->size() << "  Nodes." << endl;
	vector<Way> * way_list = new vector<Way>();
	mapDataImporter.loadWays(way_list);
	cout << "Loaded  " << way_list->size() << "  Ways." << endl;
	vector<Visit> * visit_list = new vector<Visit>();
	mapDataImporter.loadVisits(visit_list);
	std::string visit_description = mapDataImporter.generateWaysDescription(visit_list);
	cout << "Loaded " << visit_list->size() << " POI's in Visit." << endl;
	//getchar();
	// ----------------------------------------------------- IMPORTER


	// ----------------------------------------------------- GLUT
	glutInit(&argc, argv);
	glutInitDisplayMode(GLUT_DEPTH | GLUT_DOUBLE | GLUT_RGBA);
	glutInitWindowSize(640, 480);
	glutCreateWindow("Porto Visita");

	graph = new Graph();
	myInit();
	setLight();
	setMaterial();

	glutDisplayFunc(renderScene);
	glutReshapeFunc(reshape);
	glutKeyboardFunc(keyboardFunc);
	glutKeyboardUpFunc(keyboardUpFunc);
	glutTimerFunc(16, Timer, 0);

	createGraph(node_list, way_list, visit_list);
	//createGraph();

	glutMainLoop();
	// ----------------------------------------------------- GLUT
	//getchar();
	return 1;
}