#pragma once
class Visit
{
private:
	int id1_;
	int id2_;
	bool isFoot_; // true == FOOT, false == CAR
public:
	Visit(int id1, int id2, bool isFoot) : id1_(id1), id2_(id2), isFoot_(isFoot) {};
	~Visit() {};

	int getId1() { return this->id1_; };
	int getId2() { return this->id2_; };
	bool isFoot() { return this->isFoot_; };
};

