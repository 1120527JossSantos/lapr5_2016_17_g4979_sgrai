#pragma once

class Map {
private:
	float height, width;
public:
	Map(float height, float width);
	~Map();

	const float getHeight();
	const float getWidth();
};