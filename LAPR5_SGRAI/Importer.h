#pragma once

#include "Node.h"
#include "Way.h"
#include <vector>
#include <string.h>
#include <stdio.h>
#include <iomanip>      // std::setprecision
#include <stdlib.h>     /* atoi */
#include <iostream>
#include <fstream>
#include <vector>
#include "rapidxml-1.13\rapidxml.hpp"
#include "rapidjson/document.h"
#include "rapidjson/writer.h"
#include "rapidjson/stringbuffer.h"
#include <sstream>
#include <cstring>
#include "CoordinateConversor.h"
#include "Visit.h"





using namespace rapidxml;
using namespace std;
using namespace rapidjson;

static const std::string __FLAG_LOADER_TEXT_FILE = "TEXT_FILE";
static const std::string __FLAG_LOADER_API_REST = "API_REST";
static const std::string __XML_FILE = "map.xml";
static const std::string __JSON_FILE_NODES = "information.json";
static const std::string __JSON_FILE_NODE_CONNECTIONS = "Connections.json";
static const std::string __JSON_FILE_NODE_VISITS = "visit.json";

class Importer {

private:
	// conexao com a db / API Rest
	bool flagLoadData;
	bool flagNodes;
	bool flagWays;


	bool flagLoaderTextFile;
	bool flagLoaderAPIRest;




	std::vector<Node> * nodeList;
	std::vector<Way> * wayList;
	std::vector<Visit> * visitList;
	

public:
	

	Importer(/* conexao com a db / API Rest */);
	Importer(std::string LOADER_FLAG);
	~Importer();

	vector<Node> * loadData(vector<Node> * node_list);
	bool loadNodes();
	bool loadWays(vector<Way> * way_list);
	bool loadVisits(vector<Visit> * visit_list);


	std::vector<Node> getNodes();
	std::vector<Way> getWays();
	std::vector<Visit> getVisits();
	std::string generateWaysDescription(std::vector<Visit> * visit_list);

};