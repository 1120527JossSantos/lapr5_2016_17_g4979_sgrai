#pragma once

class Way {

private:
	int POI1ID_;
	int POI2ID_;
	int Distance_;
public:
	Way(int id1, int id2, int distance) : POI1ID_(id1), POI2ID_(id2), Distance_(distance) {};
	int getPOI1ID() { return POI1ID_; };
	int getPOI2ID() { return POI2ID_; };
	int getDistance() { return Distance_; };
	static std::string generateName(string id1, string id2) { return string("Connection between ") +
		string(id1) + string(" and ") +string(id2)+string("."); } // static member function
};