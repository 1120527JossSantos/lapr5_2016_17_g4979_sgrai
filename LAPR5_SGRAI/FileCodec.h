#pragma once
#include "Graph.h"

using namespace std;

class FileCodec {
public:
	virtual ~FileCodec() {};
	virtual void loadGraph() = 0;
};